import matplotlib as mpl
import matplotlib.pyplot as plt
import my_rc_params
import numpy as np
from matplotlib.ticker import MultipleLocator
from scipy.interpolate import interp1d
from scipy.optimize import minimize_scalar, root_scalar
from scipy.integrate import quad
import os

legend_fontsize = 7


def plot_rdfs():
    with mpl.rc_context(rc=my_rc_params.params):
        fig, _axes = plt.subplots(3, 2, layout="constrained", sharex="col", sharey="row")
        (axes_wz, axes_zb) = _axes.T
        temps = [270, 320, 350, 390]
        colors = "bcgr"
        linestyles = 4*["-"]
        for temp, color, ls in zip(temps, colors, linestyles):
            _plot_rdf(axes_wz, f"wz_{temp}.txt", label=f"$\SI{{{temp}}}{{\kelvin}}$", color=color, ls=ls)
            _plot_rdf(axes_zb, f"zb_{temp}.txt", label=f"$\SI{{{temp}}}{{\kelvin}}$", color=color, ls=ls)
        axes_I, axes_Ag, axes_IAg = _axes
        axes_I[0].set_ylabel(r"$g[\texttt{I-I}](r)$")
        axes_Ag[0].set_ylabel(r"$g[\texttt{Ag-Ag}](r)$")
        axes_IAg[0].set_ylabel(r"$g[\texttt{I-Ag}](r)$")
        _axes[-1, 0].set_xlabel(r"$r$ (\si{\angstrom})")
        _axes[-1, 1].set_xlabel(r"$r$ (\si{\angstrom})")
        for ax in _axes.flatten():
            ax.axhline(y=1, color="k", lw=1, ls=":")
        _axes[-1, 0].set_xlim(2.2, 10.0)
        _axes[-1, 1].set_xlim(2.2, 10.)
        for ax in _axes[-1]:
            # ax.set_ylim(-0.1, 5.0)
            ax.xaxis.set_minor_locator(MultipleLocator(0.2))
            ax.xaxis.set_major_locator(MultipleLocator(1))
        for ax in _axes[:, 0]:
            ax.yaxis.set_minor_locator(MultipleLocator(1.0))
            ax.yaxis.set_major_locator(MultipleLocator(2))
        _axes[-1, 1].legend(frameon=False, fontsize=legend_fontsize, loc="upper right", ncol=1)
        axes_wz[0].set_title(r"wz$^\ast$")
        axes_zb[0].set_title(r"zb$^\ast$")
        fig.savefig("rdfs.pdf", bbox_inches="tight")


def _plot_rdf(axes, file, lw=1, **kwargs):
    data = np.loadtxt(file)
    r = data[:, 0]
    species = [(47, 47), (53, 53), (47, 53), (53, 47)]
    rdf = {pair: g for g, pair in zip(data.T[1:], species)}
    r_peak, _ = _plot(axes[0], r, rdf[53, 53], lw=lw, **kwargs)
    _plot(axes[1], r, rdf[47, 47], lw=lw, **kwargs)
    if len(axes) == 3:
        _plot(axes[2], r, rdf[53, 47], lw=lw, **kwargs)
    # print(file, r_peak)


def _plot(ax, r, g, twinx=None, vol=None, **kwargs):
    ax.plot(r, g, **kwargs)
    # find largest peak
    r_peak, g_peak = _find_peak(r, g)
    # ax.plot([r_peak, r_peak], [-0.1, g_peak], color=kwargs["color"], lw=0.5, ls=":")
    ax.axvline(x=r_peak, color=kwargs["color"], lw=0.5, ls=":")

    if twinx is not None:
        dr = r[1] - r[0]
        rho = 0.5* np.cumsum(4*np.pi*r**2*g) * dr / vol
        twinx.plot(r[:100], rho[:100], color=kwargs["color"], lw=0.5, ls="-")
        twinx.set_ylim(0, 10)
    return r_peak, g_peak


def _find_peak(r, g, width=10):
    peak = np.argmax(g)
    f = interp1d(r[peak-width:peak+width], -g[peak-width:peak+width], kind="cubic")
    r_peak = minimize_scalar(f, bounds=(r[peak-width+1], r[peak+width-2])).x
    g_peak = -f(r_peak)
    return r_peak, g_peak


if __name__ == "__main__":
    plot_rdfs()

