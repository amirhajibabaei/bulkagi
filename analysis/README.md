# Analysis

This directory contains the analysis of the data obtained from the simulations.

First, depending on the parent configuration (wurtzite/zincblende), MD simulations are carried out for about 3ns in the NPT ensemble.
RDFs calculated from these simulations are available in the following directories:
- `rdf/`: Contains the RDFs at selected temperatures

Then, with the cell shape fixed to the average of NPT, 4ns-6ns MD simulations are carried out in the NVT ensemble.
The average iodine framework as well as the silver distribution from 4ns-6ns MD simulations are available in the following directories:
- `iodine_framework/`: Contains the average iodine framework, as well as the repeat unit analysis
- `silver_distribution/`: Silver probability density within the repeat units with grid spacing of ~0.2 Angstrom.
- `site_occupations/`: The respective repeat units where the silver sites are found from the peaks of the probability density.

For calculating the structure factors, at each temperature, 8 MD simulations are carried out in the NPT ensemble for 1ns which are followed by 1ns NVT simulations.
The resulting average structure factors are available in the following directories:
- `structure_factors/`: Structure factor analysis
