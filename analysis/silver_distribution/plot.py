import numpy as np
import pylab as plt
import matplotlib as mpl
import my_rc_params
from ase.io.cube import read_cube, write_cube


def get_cube(struc, temp, repeat=None):
    if temp == 350:
        _read = read_cube(open(f"{struc}_{temp}.cube"))
    elif temp in [390]:
        _read = read_cube(open(f"{struc}_{temp}_2x.cube"))

    uc = _read["atoms"]
    rho = _read["data"]

    uc.pbc = True
    if repeat:
        uc = uc.repeat(repeat)
        rho = np.tile(rho, repeat)
    return uc, rho



def get_(struc, temp):
    uc, rho_A = get_cube(struc, temp)
    w = (uc.cell.cellpar()[:3] / rho_A.shape)
    lz = uc.cell.cellpar()[2]
    nx, ny, nz = rho_A.shape
    X, Y, Z = np.mgrid[0:nx, 0:ny, 0:nz].astype(float)
    X *= w[0]
    Y *= w[1]
    Z *= w[2]
    closest = np.argmin(np.linalg.norm(uc.positions, axis=1))
    shift = tuple(uc.positions[closest])
    #uc.positions -= shift
    roll = np.floor(shift/ w).astype(int)
    for i, x in enumerate(roll):
        #rho_I = np.roll(rho_I, -x, axis=i)
        rho_A = np.roll(rho_A, -x, axis=i)
    return w, lz, X, Y, Z, rho_A



with mpl.rc_context(my_rc_params.params):

    fig, _axes = plt.subplots(3, 8, layout="constrained", sharex=True, sharey=True, figsize=(6.3, 2.1))

    kwargs = dict(
        # cmap = "gray_r",
        cmap = plt.cm.gist_earth_r
    )

    # tet' (wz, zb)
    temp = 350
    zmax = 8
    for struc, axes in zip("wz zb".split(), _axes[1:]):
        w, lz, X, Y, Z, rho_A = get_(struc, temp)
        axes[0].set_ylabel(r"tet$^\prime_{\rm "+struc+" }$")
        for i in range(zmax):
            z = int(i*lz/(8*w[2]))
            cm = axes[i].pcolor(X[:, :, z], Y[:, :, z], rho_A[:, :, z], vmin=0, vmax=rho_A.max(), **kwargs)

    # bcc (only wz)
    if True:
        struc = "wz"
        w, lz, X, Y, Z, rho_A = get_("wz", 390)
        axes = _axes[0]
        axes[0].set_ylabel(r"bcc")
        for i in range(zmax):
            z = int(i*lz/(8*w[2]))
            cm = axes[i].pcolor(X[:, :, z], Y[:, :, z], rho_A[:, :, z], vmin=0, vmax=rho_A.max(), **kwargs)

    for i in range(zmax):
        #  fontsize=10
        _axes[2, i].set_xlabel(f"${str(i/4)}c$")
    _axes[2, 0].set_xlabel("$0$")
    if zmax >4:
        _axes[2, 4].set_xlabel("$c$")

    #
    _axes[0, -1].set_xlabel("$2a$")
    _axes[0, -1].xaxis.set_label_position("top")
    _axes[0, -1].xaxis.tick_top()
    _axes[0, -1].set_ylabel("$2b$")
    _axes[0, -1].yaxis.set_label_position("right")
    _axes[0, -1].yaxis.tick_right()


    for ax in _axes.reshape(-1):
        ax.set_aspect("equal")
        ax.set_xticks([])
        ax.set_yticks([])
        spines = False
        ax.spines['top'].set_visible(spines)
        ax.spines['right'].set_visible(spines)
        ax.spines['bottom'].set_visible(spines)
        ax.spines['left'].set_visible(spines)


    fig.savefig("silver_distribution.pdf")
