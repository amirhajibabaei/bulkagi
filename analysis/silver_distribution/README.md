
The Ag probability distributions are provided modulo 2x2x2 tetragonal/bcc repeat units, depending on the initial configuration (wz/zb) respectively at 350K and 390K.
At 350K, the persisting distortions of I framework naturally create 2x2x2 tetragonal repeat units.
At 390K, the average iodine framework is bcc, but for a fair comparison with 350K, the Ag distributions are provided modulo 2x2x2 bcc repeat units as well.
Cross sections of the Ag distributions in the ab plane are visualized along different values of c in `silver_distribution.pdf`. 
