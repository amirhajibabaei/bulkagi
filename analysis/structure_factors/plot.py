import pylab as plt
from matplotlib.ticker import MultipleLocator
import matplotlib as mpl
import my_rc_params
import numpy as np
from ase.cell import Cell


def _get_refections(scaled, cell, k_max):
    if len(cell) == 6:
        cell = Cell.fromcellpar([a, b, c, alpha, beta, gamma]).reciprocal()
    recip = cell.reciprocal()
    q_max = int(np.floor(k_max / np.linalg.norm(2 * np.pi * recip, axis=1).min())) + 2
    q = np.array([hkl for hkl in np.ndindex(q_max, q_max, q_max)])
    f = np.exp(2j * np.pi * q @ scaled.T).sum(axis=1)
    cond = ~np.isclose(f, 0)
    # cond = abs(f) > 1e-3 #
    q = q[cond]
    k, index, count = np.unique(
        np.linalg.norm(2 * np.pi * q @ recip.T, axis=1),
        return_index=True,
        return_counts=True
    )
    mask = k < k_max
    return q[index][mask], k[mask], count[mask]


def get_reflections(lat, kmax):
    scaled = np.array([3*[0.], 3*[0.5]])
    a_bcc = 5.06
    a_tet, c_tet = [4.79, 5.58]
    cell = {
        "bcc": Cell(np.diag([a_bcc, a_bcc, a_bcc])),
        "tet": Cell(np.diag([a_tet, a_tet, c_tet]))
    }
    q, k, c = _get_refections(scaled, cell[lat], kmax)
    return q, k, c


def get_sf(struc, temp):
    sf = np.loadtxt(f"data/{struc}_{temp}_sf.txt")#[150:]
    sf = np.nan_to_num(sf)
    return sf


def plot_dummy(selection="total"):
    # params
    colors = {
            "wz": "orange",
            "zb": "dodgerblue"
        }
    kmax = 4.5
    q_bcc, *_ = get_reflections("bcc", kmax)
    q_tet, *_ = get_reflections("tet", kmax)
    pairs = [(47, 47), (53, 53), (53, 47)]
    names = {47: "Ag", 53: "I"}
    types = [f"{names[a]}-{names[b]}" for a, b in pairs] + ["total"]
    temps = reversed([270, 320, 350, 390, 450])
    ls = dict(wz="-", zb=":")

    # plot
    columns = {v: i+1 for i, v in enumerate(types)}
    col = columns[selection]
    ls = dict(wz="-", zb=":")
    with mpl.rc_context(rc=my_rc_params.params):
        fig, axes = plt.subplots(2, 1, sharex="col", layout="constrained", sharey="col", figsize=(3.2, 3))
        for i, temp in enumerate([390, 350]):
            maxes = []
            for struc in "wz zb".split():
                sf = get_sf(struc, temp)
                y = sf[:, col]
                axes[i].plot(
                        sf[:, 0],
                        y,
                        label=struc.lower()+r"$^\ast$",
                        lw=1.0,
                        #alpha=0.6,
                        color=colors[struc],
                        ls=ls[struc]
                )
                maxes.append(y.max())
            if selection != "Ag-Ag":
                axes[i].text(0.85, 0.85, f"{temp} " +r"\si{\kelvin}", rotation=0, transform=axes[i].transAxes)

        for ax in [axes[-1]]:
            ax.xaxis.set_major_locator(MultipleLocator(1.0))
            ax.xaxis.set_minor_locator(MultipleLocator(0.1))

        axes[-1].set_xlabel(r"${|\vec{k}|}$ (\si{\angstrom^{-1}})")
        axes[-1].set_ylabel(r"$S(|\vec{{k}}|)$")

        if False:
            _, k_bcc, _ = get_reflections("bcc", kmax)
            for ax in axes[0:2]:
                xxx = ax.secondary_xaxis(location="top")
                xxx.set_xticks(k_bcc)
                xxx.set_xticklabels([])
            _, k_tet, _ = get_reflections("tet", kmax)
            for ax in [axes[-1]]:
                xxx = ax.secondary_xaxis(location="top")
                xxx.set_xticks(k_tet)
                xxx.set_xticklabels([])
        else:
            _, k_bcc, _ = get_reflections("bcc", kmax)
            _, k_tet, _ = get_reflections("tet", kmax)
            k_bcc = k_bcc[1:]
            k_tet = k_tet[1:]
            kwargs = dict(s=10, edgecolor="k", facecolor="w", lw=0.5)

            for ax in axes[0:1]:
                ax.scatter(k_bcc, len(k_bcc)*[ax.get_ylim()[1]*-0.05], marker="^", label="ideal bcc", **kwargs)
            axes[1].scatter([], [], marker="^", label="ideal bcc", **kwargs)
            axes[1].scatter([], [], marker="v", label="ideal tet", **kwargs)
            for ax in [axes[-1]]:
                ax.scatter(k_tet, len(k_tet)*[ax.get_ylim()[1]*-0.05], marker="v", **kwargs)

        if selection == "total" and False:
            axes[1].text(0.8625, 13, r"$\ast$", verticalalignment="center", horizontalalignment="center")
            axes[0].text(3.9275, 10, r"$\dagger$", verticalalignment="center", horizontalalignment="center")
        if selection != "Ag-Ag":
            axes[1].legend(loc="upper left", frameon=False, ncol=2)

        if selection == "total":
            # ----
            dy = 20
            dx = 0.8

                # xlim = (0.75, 1.0)
                # ylim = (0, 3)

            for xlim, ylim in zip([(0.75, 1.0), (3.7, 4.1)], [(0, 3), (0, 5)]):
                ax_peak1 = axes[0].inset_axes([np.mean(xlim)-0.4, 18 , dx, dy], transform=axes[0].transData,
                                             xlim=xlim, ylim=ylim, xticklabels=[], yticklabels=[])
                for struc in "wz zb".split():
                    sf = get_sf(struc, 390)
                    y = sf[:, col]
                    ax_peak1.plot(
                                    sf[:, 0],
                                    y,
                                    label=struc.lower()+r"$^\ast$",
                                    lw=1.0,
                                    #alpha=0.6,
                                    color=colors[struc],
                                    ls=ls[struc]
                            )
                    ax_peak1.set_xlim(*xlim)
                    ax_peak1.set_ylim(*ylim)
                    ax_peak1.set_xticks([])
                    ax_peak1.set_yticks([])

                axes[0].indicate_inset_zoom(ax_peak1)
            # -----
            fig.savefig("structure_factor.pdf")
        else:
            fig.savefig(f"structure_factor_{selection}.pdf")

for x in "total Ag-Ag I-I".split():
    plot_dummy(x)


