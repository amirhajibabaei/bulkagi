The average iodine positions obtained from 6ns/4ns MD simulations at a few temperatures (corresponding to tet' and bcc phases) are provided in the `xyz/` folder (see `png/` folder for snapshots).
The respective repeat units are also provided in the `unit_cells/` folder. 
Reconstruction of the supercells from the given repeat units is discussed in `unit_cells/transformations/README.md`.
