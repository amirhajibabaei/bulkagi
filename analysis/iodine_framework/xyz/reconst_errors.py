from ase.io import read
import numpy as np
from ase.geometry import Cell, get_distances

for temp in [350, 390]:
    for struc in "wz zb".split():
        o = read(f"{struc}_{temp}.xyz")
        r = read(f"{struc}_{temp}_re.xyz")
        vec = (o.positions - r.positions)
        _vec, _d = get_distances(p1=o.positions, p2=r.positions, cell=o.cell, pbc=True)
        vec = _vec.diagonal().T
        d = _d.diagonal()
        assert np.allclose(np.linalg.norm(vec, axis=1), d)
        vec = vec - vec.mean(axis=0)
        disp = np.linalg.norm(vec, axis=1)
        print(struc, temp, round(disp.mean(), 3), round(disp.max(), 3))
