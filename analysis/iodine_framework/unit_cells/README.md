# Repeat Units

The repeat units of the iodine framework for structures obtained from MD simulations with wurtzite/zincblende initial configurations.
At 350K, these correspond to distorted body-centered tetragonal structures.
At 390K, these correspond to a body-centered cubic structure.
The Ag distributions are provided at `../../silver_distribution`.
