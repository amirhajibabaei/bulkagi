For reconstruction of the avergae iodine framework (see xyz folder) from the provided unitcells, the transformation matrices are aslo needed.
Aside from a rotation and permutation of atoms, the reconstructed supercell can be obtained by:
```python
import numpy as np
from ase.io import read, write
from ase.build import make_supercell

# Read the unitcell
unitcell = read('unitcell.cif')
transformation_matrix = np.load('transformation_matrix.npy')
re_supercell = make_supercell(unit_cell, transformation_matrix.T)

```
These reconstructions are provided in the xyz folder.
