# Average Iodine Framework

Snapshots of the average iodine positions from 4ns/6ns MD simulations at 350K/390K.
At 350K, a distorted body-centered tetragonal (bct) structure is observed (with distortions depending on the parents), while at 390K, a body-centered cubic (bcc) structure is observed.
