# Repeat Units

The repeat units for structures obtained from MD simulations with wurtzite/zincblende initial configurations.
At 350K, these correspond to distorted body-centered tetragonal structures.
At 390K, these correspond to a body-centered cubic structure.
In the `cif` files, the average positions of the I atoms are given as well as the most likely positions of the Ag atoms.
The Ag sites are obtained from the local peaks of the probability distribution of the Ag atoms (see `../silver_distribution`).
The site occupancy in the `cif` files is proportional to the height of the peaks in the probability distribution.


A clustering analysis with `ovito` is provided through the `visual.ovito` which uses a shading to indicate the prominence of sites as well as a cutoff distance (2.6 angstroms) to indicate the dominant cluster (see `wz-derived-tetragonal.png` for an example).
