# Supporting Information

This repository contains additional supporting information for the publication titled "Symmetry Breaking in the Superionic Phase of Silver Iodide", Phys. Rev. Lett. 134, 026306 (2025) [DOI:10.1103/PhysRevLett.134.026306](https://doi.org/10.1103/PhysRevLett.134.026306).
- The trained machine learning force-fields (MACE checkpoints) are stored in the `models` directory.
- The training DFT data are published in Zenodo [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14282509.svg)](https://doi.org/10.5281/zenodo.14282509).
- Analysis and visualization scripts are available in the `analysis` directory.



